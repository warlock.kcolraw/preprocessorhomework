#include <cmath>
#pragma once

double SumThenPow(double FirstDouble, double SecondDouble)
{
	return pow(FirstDouble + SecondDouble, 2);
}